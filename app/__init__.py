import os
from flask_restplus import Api
from flask import Blueprint
from .main.controller.user_controller import api as user_ns
from .main.controller.auth_controller import api as auth_auth_ns
from .main.controller.friend_list_controller import api as friend_list_ns
from .main.controller.user_profile_controller import api as user_profile_ns
from .main.controller.user_publication_controller import api as user_publication_ns
from .main.controller.user_publication_feed_controller import api as user_publication_feed_ns
from .main.controller.user_publication_friends import api as user_publication_friend_ns

blueprint = Blueprint('api', __name__)

api = Api(blueprint,
          title='FLASK RESTPLUS API BOILER-PLATE WITH JWT',
          version='1.0',
          description='a boilerplate for flask restplus web service',
          doc=False
          #doc='/' if os.getenv('BOILERPLATE_ENV') == 'dev' else False
          )

api.add_namespace(user_ns, path='/user')
api.add_namespace(auth_auth_ns)
api.add_namespace(friend_list_ns)
api.add_namespace(user_profile_ns)
api.add_namespace(user_publication_ns, path='/publication')
api.add_namespace(user_publication_feed_ns)
api.add_namespace(user_publication_friend_ns)
