from flask import request
from flask_restplus import Resource

from app.main import HTTP_RESPONSE_CODE, HTTP_RESPONSE
from app.main.service.publication_service import get_publication, create_publication, delete_publication, update_publication_information

from ..util.dto import PublicationDto

api = PublicationDto.api
_publication = PublicationDto.publication
_publication_stack = PublicationDto.publication_stack


@api.route('/')
class Publication(Resource):

    @api.doc('publishing a new publication')
    @api.header('access_token', 'auth token')
    @api.expect(_publication)
    def post(self):
        data = request.json
        token = request.headers.get('access_token')
        return create_publication(token, data)

    @api.doc('get published publication for a user')
    @api.header('access_token', 'auth token')
    @api.expect(_publication)
    @api.marshal_with(_publication_stack)
    def get(self):
        token = request.headers.get('access_token')
        id = request.headers.get('id') or None
        page = request.headers.get('page') or 1
        page_size = request.headers.get('page_size') or 5
        return get_publication(token, id, int(page), int(page_size))

    @api.doc('update a publication')
    @api.header('access_token', 'auth token')
    @api.expect(_publication)
    def put(self):
        token = request.headers.get('access_token')
        data = request.json
        return update_publication_information(token, data)

    @api.doc('auth a user and create a access token')
    @api.header('access_token', 'auth token')
    @api.header('id', 'publication id')
    def delete(self):
        token = request.headers.get('access_token')
        publication_id = request.headers.get('id')
        return delete_publication(token, publication_id)

