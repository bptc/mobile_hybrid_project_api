from flask import request
from flask_restplus import Resource

from app.main import HTTP_RESPONSE_CODE, HTTP_RESPONSE
from app.main.service.publication_service import get_friend_publication, delete_reaction, add_reaction

from ..util.dto import PublicationDto

api = PublicationDto.api
_publication_reaction = PublicationDto.publication_reaction
_publication_stack = PublicationDto.publication_stack


@api.route('/friend')
class Feed(Resource):

    @api.doc('get published publication from a specific friend')
    @api.header('access_token', 'auth token')
    @api.header('page', 'feed page')
    @api.header('friend_id', 'feed page')
    @api.header('page_size', 'feed page')
    @api.marshal_with(_publication_stack)
    def get(self):
        token = request.headers.get('access_token')
        friend_id = request.headers.get('friend_id')
        page = request.headers.get('page') or 1
        page_size = request.headers.get('page_size') or 5
        if not friend_id:
            return {"status": HTTP_RESPONSE['not_found']}, HTTP_RESPONSE_CODE['not_found']
        return get_friend_publication(token, int(friend_id), int(page), int(page_size))

    @api.doc("Adding a new reaction to a friend publication")
    @api.header('access_token', 'auth token')
    @api.expect(_publication_reaction, validate=True)
    def post(self):
        token = request.headers.get('access_token')
        data = request.json
        return add_reaction(token, data)


    @api.doc("delete a reaction to a friend publication")
    @api.header('access_token', 'auth token')
    @api.header('user_publication_id', 'feed page')
    @api.header('user_id', 'feed page')
    def delete(self, instance):
        token = request.headers.get('access_token')
        user_publication_id = request.headers.get('user_publication_id')
        user_id = request.headers.get('user_id')
        if not user_id or not user_publication_id:
            return {"status": HTTP_RESPONSE['bad_request']}, HTTP_RESPONSE_CODE['bad_request']
        return delete_reaction(token, user_id, user_publication_id)
