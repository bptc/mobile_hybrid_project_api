from flask import request
from flask_restplus import Resource

from app.main import HTTP_RESPONSE_CODE, HTTP_RESPONSE

from ..util.dto import AuthDto
from ..service.auth_service import login, logout

api = AuthDto.api
_user_auth = AuthDto.user_auth


@api.route('/login')
class User(Resource):

    @api.doc('auth a user and create a access token')
    @api.expect(_user_auth, validate=True)
    def post(self):
        data = request.json
        return login(email=data['email'], password=data['password'])


@api.route('/logout')
class User(Resource):

    @api.doc('disconect a user with a access token')
    @api.header('access_token', 'auth token')
    def post(self):
        token = request.headers["access_token"]
        if not token:
            return {"status": HTTP_RESPONSE["unauthorized"]}, HTTP_RESPONSE_CODE['unauthorized']
        flag, code, message = logout(token)
        response_obj = {
            "status": message if not flag else HTTP_RESPONSE["ok"]
        }
        return response_obj, code
