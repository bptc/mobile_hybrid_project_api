from flask import request
from flask_restplus import Resource

from app.main import HTTP_RESPONSE_CODE, HTTP_RESPONSE

from ..util.dto import UserDto
from ..service.user_service import save_new_user, delete_user, get_matching_user

api = UserDto.api
_user = UserDto.user
_user_finder = UserDto.user_finder


@api.route('/')
class User(Resource):

    @api.response(HTTP_RESPONSE_CODE["ok"], HTTP_RESPONSE["ok"])
    @api.doc('create a new user')
    @api.expect(_user, validate=True)
    def post(self):
        data = request.json
        return save_new_user(data=data)

    @api.doc('get list of user matching the query')
    @api.header('access_token', 'auth token')
    @api.header('query', 'search query')
    @api.marshal_with(_user_finder)
    def get(self):
        token = request.headers.get('access_token')
        query = request.headers.get('query')
        if not query:
            return {"status": HTTP_RESPONSE['ok']}, HTTP_RESPONSE_CODE['ok']
        return get_matching_user(token, query)

    @api.doc('delete a user')
    @api.header('access_token', 'auth token')
    def delete(self):
        token = request.headers.get('access_token')
        return delete_user(token)
