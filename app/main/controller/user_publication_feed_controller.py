from flask import request
from flask_restplus import Resource

from app.main import HTTP_RESPONSE_CODE, HTTP_RESPONSE
from app.main.service.publication_service import get_feed

from ..util.dto import PublicationDto

api = PublicationDto.api
_publication = PublicationDto.publication
_publication_stack = PublicationDto.publication_stack


@api.route('/feed')
class Feed(Resource):

    @api.doc('get published feed')
    @api.header('access_token', 'auth token')
    @api.header('page', 'feed page')
    @api.header('page_size', 'feed page')
    @api.marshal_with(_publication_stack)
    def get(self):
        token = request.headers.get('access_token')
        page = request.headers.get('page') or 1
        page_size = request.headers.get('page_size') or 5
        return get_feed(token, int(page), int(page_size))

