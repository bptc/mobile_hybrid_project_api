from flask import request
from flask_restplus import Resource
from app.main.model.user import User

from ..util.dto import UserDto
from ..service.friend_list_service import delete_friend, get_friend_list, confirm_friend_request, add_friend_request

api = UserDto.api
_user_friend = UserDto.friend_list


@api.route('/friends')
class Friends(Resource):

    @api.doc('accept friend request')
    @api.header('access_token', 'auth token')
    def post(self):
        friend_id = request.json['friend_id']
        flag, code, result = User.decode_auth_token(request.headers.get('access_token'))
        if not flag:
            return result, code
        return confirm_friend_request(result, friend_id)

    @api.doc('get friend list and waiting friend request, must be donne once per connection')
    @api.header('access_token', 'auth token')
    @api.marshal_with(_user_friend)
    def get(self):
        flag, code, result = User.decode_auth_token(request.headers.get('access_token'))
        if not flag:
            return result, code
        return get_friend_list(result)

    @api.doc('remove friend from friend list/or friend request')
    @api.header('access_token', 'auth token')
    @api.header('friend_id', 'the user i want to unfriend')
    def delete(self):
        friend_id = request.headers.get('friend_id')
        flag, code, result = User.decode_auth_token(request.headers.get('access_token'))
        if not flag:
            return result, code
        return delete_friend(result, friend_id)

    # todo: add here push notification with expo
    @api.doc('send friend request')
    @api.header('access_token', 'auth token')
    def put(self):
        friend_id = request.json['friend_id']
        flag, code, result = User.decode_auth_token(request.headers.get('access_token'))
        if not flag:
            return result, code
        return add_friend_request(result, friend_id)
