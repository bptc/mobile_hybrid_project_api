from flask import request
from flask_restplus import Resource
from app.main.model.user import User

from ..util.dto import UserDto
from ..service.user_profile_service import get_user_info, update_user_info

api = UserDto.api
_user_secure = UserDto.user_secure
_user = UserDto.user


@api.route('/profile')
class Profile(Resource):
    @api.doc('update user profile information')
    @api.header('access_token', 'auth token')
    @api.expect(_user)
    def put(self):
        token = request.headers.get('access_token')
        flag, code, result = User.decode_auth_token(token)
        data = request.json
        if not flag:
            return result, code
        return update_user_info(result, data)

    @api.doc('get user profile information')
    @api.header('access_token', 'auth token')
    @api.header('user_id', 'id of a specific user')
    @api.marshal_with(_user_secure)
    def get(self):
        token = request.headers.get('access_token')
        flag, code, result = User.decode_auth_token(token)
        if not flag:
            return result, code
        id = request.headers.get('user_id') or result
        return get_user_info(id)

