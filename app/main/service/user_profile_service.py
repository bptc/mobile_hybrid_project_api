from app.main import db, HTTP_RESPONSE, HTTP_RESPONSE_CODE
from app.main.model.user import User
from app.main.model.user_publication import UserPost
from .user_service import commit_change
from sqlalchemy import func


@commit_change
def update_user_info(user_id, information):
    user = User.query.filter_by(id=user_id).first()
    if not user:
        return {"status": HTTP_RESPONSE['not_found']}, HTTP_RESPONSE_CODE['not_found']
    if 'email' in information and information['email']:
        user.email = information['email']
    if 'first_name' in information and information['first_name']:
        user.first_name = information['first_name']
    if 'last_name' in information and information['last_name']:
        user.last_name = information['last_name']
    if 'password' in information and information['password']:
        user.password = information['password']
    if 'profile_picture' in information and information['profile_picture']:
        user.profile_picture = information['profile_picture']
    return {"status": HTTP_RESPONSE['ok']}, HTTP_RESPONSE_CODE['ok'],


def get_user_info(user_id):
    user = User.query.filter_by(id=user_id).first()
    if not user:
        return {"status": HTTP_RESPONSE['not_found']}, HTTP_RESPONSE_CODE['not_found']
    user_post_number = UserPost.query.filter_by(user_id=user.id).count()
    user_post_like_number = db.session.query(func.sum(UserPost.like)).filter(UserPost.user_id == user_id).scalar()
    user_friend_number = len(user.friend_list)
    user.publication_number = user_post_number or 0
    user.friend_number = user_friend_number or 0
    user.publication_like_number = user_post_like_number or 0
    return user, HTTP_RESPONSE_CODE['ok']