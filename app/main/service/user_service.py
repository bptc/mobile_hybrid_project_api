import datetime
from app.main import db, HTTP_RESPONSE, HTTP_RESPONSE_CODE
from app.main.model.user import User
from app.main.model.user_publication import UserPost
from sqlalchemy import func


def commit_change(function):
    def wrapper(*args, **kwargs):
        result = function(*args)
        db.session.commit()
        return result
    return wrapper


@commit_change
def save_changes(data):
    db.session.add(data)


def save_new_user(data):
    user = User.query.filter_by(email=data['email']).first()
    if not user:
        new_user = User(
            email=data['email'],
            first_name=data['first_name'],
            last_name=data['last_name'],
            username=data['username'],
            password=data['password'],
            profile_picture=data['profile_picture'] if "profile_picture" in data and data['profile_picture'] else None,
            registered_on=datetime.datetime.utcnow()
        )
        save_changes(new_user)
        response_object = {
            'status': 'success',
            'message': HTTP_RESPONSE["ok"]
        }
        return response_object, HTTP_RESPONSE_CODE["ok"]
    else:
        response_object = {
            'status': 'fail',
            'message': HTTP_RESPONSE["conflict"],
        }
        return response_object, HTTP_RESPONSE_CODE["conflict"]


def get_matching_user(token, query):
    flag, code, result = User.decode_auth_token(token)
    if not flag:
        return result, code
    query = "{}%".format(query)
    research_result = User.query.filter(
        User.id.isnot(result) &
        (User.email.like(query) |
         User.username.like(query) |
         User.last_name.like(query) |
         User.first_name.like(query))
    ).all()
    if not research_result:
        return {"status": HTTP_RESPONSE['not_found']}, HTTP_RESPONSE_CODE['not_found']
    user_list_len = len(research_result)
    for index in range(user_list_len):
        user_post_number = UserPost.query.filter_by(user_id=research_result[index].id).count()
        user_friend_number = len(research_result[index].friend_list)
        user_post_like_number = db.session.query(func.sum(UserPost.like)).filter(UserPost.user_id == research_result[index].id).scalar()
        research_result[index].publication_number = user_post_number or 0
        research_result[index].friend_number = user_friend_number or 0
        research_result[index].publication_like_number = user_post_like_number or 0
    return {"status": HTTP_RESPONSE['ok'], "user_list": research_result}, HTTP_RESPONSE_CODE['ok']


@commit_change
def delete_user(token):
    flag, code, result = User.decode_auth_token(token)
    if not flag:
        return result, code
    user = User.query.filter_by(id=result).first()
    if not user:
        return {"status": HTTP_RESPONSE['not_found']}, HTTP_RESPONSE_CODE['not_found']
    db.session.delete(user)
    return {"status": HTTP_RESPONSE['ok']}, HTTP_RESPONSE_CODE['ok']
