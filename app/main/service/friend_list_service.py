from app.main import db, HTTP_RESPONSE, HTTP_RESPONSE_CODE
from app.main.model.user import User
from app.main.service.user_service import commit_change


@commit_change
def delete_friend(user_id, friend_id):
    user = User.query.filter_by(id=user_id).first()
    user_to_unfriend = User.query.filter_by(id=friend_id).first()
    code, return_obj = user.remove_friend(user_to_unfriend)
    if not code == HTTP_RESPONSE_CODE['ok']:
        code, return_obj = user.remove_friend_request(user_to_unfriend)
    return code, return_obj


@commit_change
def add_friend_request(user_id, friend_id):
    user = User.query.filter_by(id=user_id).first()
    user_to_request = User.query.filter_by(id=friend_id).first()
    return user.ad_friend(user_to_request)


@commit_change
def confirm_friend_request(user_id, friend_id):
    user = User.query.filter_by(id=user_id).first()
    user_to_add = User.query.filter_by(id=friend_id).first()
    return user.confirm_friend_request(user_to_add)


@commit_change
def get_friend_list(user_id):
    user = User.query.filter_by(id=user_id).first()
    format_response = {
        "status": HTTP_RESPONSE['ok'],
        "friend_request_receive": user.friend_request_receive,
        "friend_request_send": user.friend_request_send,
        "friend_list": user.friend_list
    }
    return format_response, HTTP_RESPONSE_CODE['ok']

