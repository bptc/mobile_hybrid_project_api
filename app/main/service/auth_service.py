from app.main.model.user import User
from app.main import HTTP_RESPONSE, HTTP_RESPONSE_CODE


def login(email, password):
    try:
        user = User.query.filter_by(email=email).first()
        if user and user.check_password(password):
            access_token = User.encode_auth_token(user.id)
            return {
                'status': HTTP_RESPONSE['ok'],
                'access_token': access_token.decode()
            },  HTTP_RESPONSE_CODE['ok']
        return {'status': HTTP_RESPONSE_CODE['unauthorized']}, HTTP_RESPONSE_CODE['unauthorized']
    except Exception as e:
        return {'status': HTTP_RESPONSE['server_error']}, HTTP_RESPONSE_CODE['server_error']


def logout(access_token):
    return User.decode_auth_token(access_token)
