import datetime

from sqlalchemy import text

from app.main import db, HTTP_RESPONSE, HTTP_RESPONSE_CODE
from .user_service import commit_change, save_changes
from app.main.model.user import User
from app.main.model.user_publication import UserPost


@commit_change
def create_publication(token, data):
    flag, code, result = User.decode_auth_token(token)
    if not flag:
        return result, code
    user = User.query.filter_by(id=result).first()
    if not user:
        return {"status": HTTP_RESPONSE['not_found']}, HTTP_RESPONSE_CODE['not_found']
    new_post = UserPost(
        description=data['description'],
        publication_content=data['publication_content'],
        publication_type=data['publication_type'],
        user_id=user.id,
        creation_date=datetime.datetime.utcnow(),
        title=data['title']
    )
    save_changes(new_post)
    return HTTP_RESPONSE_CODE['ok'], {"status": HTTP_RESPONSE['ok']}


@commit_change
def delete_publication(token, post_id):
    flag, code, result = User.decode_auth_token(token)
    if not flag:
        return result, code
    user = User.query.filter_by(id=result).first()
    if not user:
        return {"status": HTTP_RESPONSE['not_found']}, HTTP_RESPONSE_CODE['not_found']
    publication = UserPost.query.filter(UserPost.id.is_(post_id) & UserPost.user_id.is_(user.id)).first()
    if not publication:
        return {"status": HTTP_RESPONSE['not_found']}, HTTP_RESPONSE_CODE['not_found']
    db.session.delete(publication)
    return HTTP_RESPONSE_CODE['ok'], {"status": HTTP_RESPONSE['ok']}


@commit_change
def update_publication_information(token, data):
    flag, code, result = User.decode_auth_token(token)
    if not flag:
        return result, code
    user = User.query.filter_by(id=result).first()
    if not user:
        return {"status": HTTP_RESPONSE['not_found']}, HTTP_RESPONSE_CODE['not_found']
    if 'id' not in data:
        return {"status": HTTP_RESPONSE['bad_request']}, HTTP_RESPONSE_CODE['bad_request']
    publication = UserPost.query.filter(UserPost.id.is_(data['id']) & UserPost.user_id.is_(user.id)).first()
    if not publication:
        return {"status": HTTP_RESPONSE['not_found']}, HTTP_RESPONSE_CODE['not_found']
    if 'description' in data:
        publication.description = data['description']
    if 'publication_content' in data:
        publication.publication_content = data['publication_content']
    if 'publication_type' in data:
        publication.publication_type = data['publication_type']
    if 'title' in data:
        publication.title = data['title']
    return {"status": HTTP_RESPONSE['ok']}, HTTP_RESPONSE_CODE['ok']


def get_publication(token, publication_id, page, page_size):
    flag, code, result = User.decode_auth_token(token)
    if not flag:
        return result, code
    user = User.query.filter_by(id=result).first()
    if not user:
        return {"status": HTTP_RESPONSE['not_found']}, HTTP_RESPONSE_CODE['not_found']
    if publication_id:
        publication = UserPost.query.filter(UserPost.id.is_(publication_id) & UserPost.user_id.is_(user.id)).all()
    else:
        publication = UserPost.query.filter_by(user_id=user.id)\
            .order_by(UserPost.creation_date.desc()).paginate(page, per_page=page_size)
        publication = publication.items
    if not publication:
        return {"status": HTTP_RESPONSE['not_found']}, HTTP_RESPONSE_CODE['not_found']
    return {"status": HTTP_RESPONSE['ok'], 'publications': publication}, HTTP_RESPONSE_CODE['ok']


def get_feed(token, page, page_size):
    flag, code, result = User.decode_auth_token(token)
    if not flag:
        return result, code
    user = User.query.filter_by(id=result).first()
    if not user:
        return {"status": HTTP_RESPONSE['not_found']}, HTTP_RESPONSE_CODE['not_found']
    user_friend = user.friend_list
    user_friend_id = [result]
    for item in user_friend:
        user_friend_id.append(item.id)
    publication = UserPost.query.filter(UserPost.user_id.in_(user_friend_id)).\
        order_by(UserPost.creation_date.desc()).paginate(page, per_page=page_size)
    if publication:
        publication = publication.items
        number_of_publication = len(publication)
        for index in range(number_of_publication):
            if user in publication[index].who_like:
                publication[index].doIlikeIt = True
            elif user in publication[index].who_dislike:
                publication[index].doIdontlikeIt = True
        return {"status": HTTP_RESPONSE['ok'], 'publications': publication}, HTTP_RESPONSE_CODE['ok']
    return {"status": HTTP_RESPONSE['not_found'], 'publications': []}, HTTP_RESPONSE_CODE['not_found']


def get_friend_publication(token, friend_id, page, page_size):
    flag, code, result = User.decode_auth_token(token)
    if not flag:
        return result, code
    user = User.query.filter_by(id=result).first()
    if not user:
        return {"status": HTTP_RESPONSE['not_found']}, HTTP_RESPONSE_CODE['not_found']
    user_friend = User.query.filter_by(id=friend_id).first()
    if user_friend in user.friend_list:
        publication = UserPost.query.filter(UserPost.user_id.is_(friend_id)). \
            order_by(UserPost.creation_date.desc()).paginate(page, per_page=page_size)
        publication = publication.items
        number_of_publication = len(publication)
        for index in range(number_of_publication):
            if user in publication[index].who_like:
                publication[index].doIlikeIt = True
            elif user in publication[index].who_dislike:
                publication[index].doIdontlikeIt = True
        return {"status": HTTP_RESPONSE['ok'], 'publications': publication}, HTTP_RESPONSE_CODE['ok']
    return {"status": HTTP_RESPONSE['unauthorized'], 'publications': []}, HTTP_RESPONSE_CODE['unauthorized']


@commit_change
def add_reaction(token, data):
    flag, code, result = User.decode_auth_token(token)
    publication = UserPost.query.filter_by(id=data["user_publication_id"]).first()
    user = User.query.filter_by(id=result).first()

    if not flag:
        return result, code
    if not user or not publication:
        return {"status": HTTP_RESPONSE['not_found']}, HTTP_RESPONSE_CODE['not_found']
    if data["reaction"] == "like":
        publication.new_like(user)
        return {"status": HTTP_RESPONSE['ok']}, HTTP_RESPONSE_CODE['ok']
    elif data["reaction"] == "dislike":
        publication.new_dislike(user)
        return {"status": HTTP_RESPONSE['ok']}, HTTP_RESPONSE_CODE['ok']
    return {"status": HTTP_RESPONSE['not_allowed']}, HTTP_RESPONSE_CODE['not_allowed']


@commit_change
def delete_reaction(token, publication_id):
    flag, code, result = User.decode_auth_token(token)
    publication = UserPost.query.filter_by(id=publication_id).first()
    user = User.query.filter_by(id=result).first()

    if not flag:
        return result, code
    if not user or not publication:
        return {"status": HTTP_RESPONSE['not_found']}, HTTP_RESPONSE_CODE['not_found']
    publication.remove_like(user)
    publication.remove_dislike(user)
    return {"status": HTTP_RESPONSE['ok']}, HTTP_RESPONSE_CODE['ok']
