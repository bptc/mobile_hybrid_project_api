from app.main import db, flask_bcrypt, HTTP_RESPONSE_CODE, HTTP_RESPONSE
import datetime
import jwt
from ..config import key

friendship = db.Table(
    'UserFriendList',
    db.Column('id_user', db.Integer, db.ForeignKey('user.id'), primary_key=True),
    db.Column('id_friend', db.Integer, db.ForeignKey('user.id'), primary_key=True),
    db.UniqueConstraint('id_user', 'id_friend', name='unique_friendships')
)

friendship_request_receive = db.Table(
    'UserFriendResquestReceive',
    db.Column('id_user_sender', db.Integer, db.ForeignKey('user.id'), primary_key=True),
    db.Column('id_friend_requested', db.Integer, db.ForeignKey('user.id'), primary_key=True),
    db.UniqueConstraint('id_user_sender', 'id_friend_requested', name='unique_friendships')
)

friendship_request_send = db.Table(
    'UserFriendResquestSend',
    db.Column('id_user_sender', db.Integer, db.ForeignKey('user.id'), primary_key=True),
    db.Column('id_friend_requested', db.Integer, db.ForeignKey('user.id'), primary_key=True),
    db.UniqueConstraint('id_user_sender', 'id_friend_requested', name='unique_friendships')
)


class User(db.Model):

    __tablename__ = "user"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    profile_picture = db.Column(db.TEXT)
    email = db.Column(db.String(255), unique=True, nullable=False)
    first_name = db.Column(db.String(100), nullable=False)
    last_name = db.Column(db.String(100), nullable=False)
    username = db.Column(db.String(50), unique=True, nullable=False)
    password_hashed = db.Column(db.String(100))
    registered_on = db.Column(db.DateTime, nullable=False)
    friend_list = db.relationship(
        'User', secondary=friendship,
        primaryjoin=id == friendship.c.id_user,
        secondaryjoin=id == friendship.c.id_friend
    )
    friend_request_receive = db.relationship(
        'User', secondary=friendship_request_receive,
        primaryjoin=id == friendship_request_receive.c.id_user_sender,
        secondaryjoin=id == friendship_request_receive.c.id_friend_requested
    )
    friend_request_send = db.relationship(
        'User', secondary=friendship_request_send,
        primaryjoin=id == friendship_request_send.c.id_user_sender,
        secondaryjoin=id == friendship_request_send.c.id_friend_requested
    )
    user_post = db.relationship('UserPost', backref='user', lazy=True)
    publication_number = 0
    publication_like_number = 0
    friend_number = 0

    @property
    def password(self):
        raise AttributeError('password: write-only field')

    def confirm_friend_request(self, friend):
        if friend.id == self.id:
            return HTTP_RESPONSE_CODE['not_allowed'], {"status": HTTP_RESPONSE['not_allowed']}
        if friend in self.friend_request_receive:
            self.friend_request_receive.remove(friend)
            friend.friend_request_send.remove(self)
            self.friend_list.append(friend)
            friend.friend_list.append(self)
            return HTTP_RESPONSE_CODE['ok'], {"status": HTTP_RESPONSE['ok']}
        return HTTP_RESPONSE_CODE['not_found'], {"status": HTTP_RESPONSE['not_found']}

    def remove_friend_request(self, friend):
        if friend.id == self.id:
            return HTTP_RESPONSE_CODE['not_allowed'], {"status": HTTP_RESPONSE['not_allowed']}
        if friend in self.friend_request_receive:
            self.friend_request_receive.remove(friend)
            friend.friend_request_send.remove(self)
            return HTTP_RESPONSE_CODE['ok'], {"status": HTTP_RESPONSE['ok']}
        elif friend in self.friend_request_send:
            self.friend_request_send.remove(friend)
            friend.friend_request_receive.remove(self)
            return HTTP_RESPONSE_CODE['ok'], {"status": HTTP_RESPONSE['ok']}
        return HTTP_RESPONSE_CODE['not_found'], {"status": HTTP_RESPONSE['not_found']}

    def ad_friend(self, friend):
        if friend.id == self.id or friend.id in self.friend_request_receive or \
                friend.id in self.friend_request_send or friend.id in self.friend_list:
            return HTTP_RESPONSE_CODE['not_allowed'], {"status": HTTP_RESPONSE['not_allowed']}
        if friend not in self.friend_list:
            self.friend_request_send.append(friend)
            friend.friend_request_receive.append(self)
            return HTTP_RESPONSE_CODE['ok'], {"status": HTTP_RESPONSE['ok']}
        return HTTP_RESPONSE_CODE['not_found'], {"status": HTTP_RESPONSE['not_found']}

    def remove_friend(self, friend):
        if friend.id == self.id:
            return HTTP_RESPONSE_CODE['not_allowed'], {"status": HTTP_RESPONSE['not_allowed']}
        if friend in self.friend_list:
            self.friend_list.remove(friend)
            friend.friend_list.remove(self)
            return HTTP_RESPONSE_CODE['ok'], {"status": HTTP_RESPONSE['ok']}
        return HTTP_RESPONSE_CODE['not_found'], {"status": HTTP_RESPONSE['not_found']}


    @password.setter
    def password(self, password):
        self.password_hashed = flask_bcrypt.generate_password_hash(password).decode('utf-8')

    def check_password(self, password):
        return flask_bcrypt.check_password_hash(self.password_hashed, password)

    @staticmethod
    def encode_auth_token(user_id):
        """
        Generates the Auth Token
        :return: string
        """
        try:
            payload = {
                'exp': datetime.datetime.utcnow() + datetime.timedelta(days=1),
                'iat': datetime.datetime.utcnow(),
                'user_id': user_id
            }
            return jwt.encode(
                payload,
                key,
                algorithm='HS256'
            )
        except Exception as e:
            return e

    @staticmethod
    def decode_auth_token(auth_token):
        try:
            payload = jwt.decode(auth_token, key)
            return True, HTTP_RESPONSE_CODE['ok'], payload['user_id']
        except jwt.ExpiredSignatureError:
            return False, HTTP_RESPONSE_CODE['token_expired'], HTTP_RESPONSE['token_expired']
        except jwt.InvalidTokenError:
            return False, HTTP_RESPONSE_CODE['token_expired'], HTTP_RESPONSE['token_expired']

    def __repr__(self):
        return "<User '{}'>".format(self.username)