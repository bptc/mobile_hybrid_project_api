from app.main import db, flask_bcrypt, HTTP_RESPONSE_CODE, HTTP_RESPONSE
import datetime
import jwt
from ..config import key

publication_lover = db.Table(
    'publication_lover',
    db.Column('id_user', db.Integer, db.ForeignKey('user.id'), primary_key=True),
    db.Column('publication_id', db.Integer, db.ForeignKey('user_post.id'), primary_key=True),
)


publication_hater = db.Table(
    'publication_hater',
    db.Column('id_user', db.Integer, db.ForeignKey('user.id'), primary_key=True),
    db.Column('publication_id', db.Integer, db.ForeignKey('user_post.id'), primary_key=True),
)


# One to many representation
class UserPost(db.Model):

    __tablename__ = "user_post"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    description = db.Column(db.String(255))
    like = db.Column(db.Integer, default=0)
    dislike = db.Column(db.Integer, default=0)
    publication_content = db.Column(db.TEXT)
    publication_type = db.Column(db.String(100), nullable=False)
    title = db.Column(db.String(100), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    creation_date = db.Column(db.DateTime, nullable=False)
    who_like = db.relationship('User', secondary=publication_lover, lazy='subquery')
    who_dislike = db.relationship('User', secondary=publication_hater, lazy='subquery')
    doIlikeIt = False
    doIdontlikeIt = False

    def new_like(self, user):
        if user in self.who_like:
            self.remove_like(user)
        else:
            if user in self.who_dislike:
                self.remove_dislike(user)
            self.who_like.append(user)
            self.like += 1
        return {"status": HTTP_RESPONSE['ok']}, HTTP_RESPONSE_CODE['ok']

    def new_dislike(self, user):
        if user in self.who_dislike:
            self.remove_dislike(user)
        else:
            if user in self.who_like:
                self.remove_like(user)
            self.who_dislike.append(user)
            self.dislike += 1
        return {"status": HTTP_RESPONSE['ok']}, HTTP_RESPONSE_CODE['ok']

    def remove_like(self, user):
        if user in self.who_like:
            self.who_like.remove(user)
            self.dislike -= 1
        else:
            return HTTP_RESPONSE_CODE['not_found'], {"status": HTTP_RESPONSE['not_found']}
        return {"status": HTTP_RESPONSE['ok']}, HTTP_RESPONSE_CODE['ok']

    def remove_dislike(self, user):
        if user in self.who_dislike:
            self.who_dislike.remove(user)
            self.dislike -= 1
        else:
            return HTTP_RESPONSE_CODE['not_found'], {"status": HTTP_RESPONSE['not_found']}
        return {"status": HTTP_RESPONSE['ok']}, HTTP_RESPONSE_CODE['ok']

    def __repr__(self):
        return "<User '{}'>".format(self.title)
