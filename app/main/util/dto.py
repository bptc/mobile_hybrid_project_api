from flask_restplus import Namespace, fields


class UserDto:
    api = Namespace('user', description='user related operations')
    user = api.model('user', {
        'email': fields.String(required=True, description='user email address'),
        'username': fields.String(required=True, description='user username'),
        'password': fields.String(required=True, description='user password'),
        'first_name': fields.String(required=True, description='user first name'),
        'last_name': fields.String(required=True, description='user last name'),
        'profile_picture': fields.String(description='user pictures in base 64'),
    })
    user_secure = api.model('user_secure', {
        'id': fields.Integer(description='user id'),
        'profile_picture': fields.String(description='user pictures in base 64'),
        'email': fields.String(description='user email address'),
        'username': fields.String(description='user username'),
        'first_name': fields.String(description='user first name'),
        'last_name': fields.String(description='user last name'),
        'publication_number': fields.Integer(description='number of publication'),
        'publication_like_number': fields.Integer(description='number of like'),
        'friend_number': fields.Integer(description='number of friend')
    })
    friend_list = api.model("friend_list", {
        'status': fields.String(required=True, description='request status'),
        'friend_request_receive': fields.List(fields.Nested(user_secure), description="list of all friend request"),
        'friend_request_send': fields.List(fields.Nested(user_secure), description="list of all friend request"),
        'friend_list': fields.List(fields.Nested(user_secure), description="list of all friend"),
    })
    user_finder = api.model("user_finder", {
        'status': fields.String(description='request status'),
        'user_list': fields.List(fields.Nested(user_secure), description="list of all user found"),
    })


class AuthDto:
    api = Namespace('auth', description='authentication related operations')
    user_auth = api.model('auth_details', {
        'email': fields.String(required=True, description='The email address'),
        'password': fields.String(required=True, description='The user password '),
        'expo_token': fields.String(description='The device expo push token')
    })


class PublicationDto:
    api = Namespace('publication', description='authentication related operations')
    publication = api.model('auth_details', {
        'title': fields.String(required=True, description='Publication title'),
        'description': fields.String(required=True, description='Publication description'),
        'publication_content': fields.String(required=True, description='publication content'),
        'id': fields.Integer(description='publication id'),
        'like': fields.Integer(description='number of like on a publication'),
        'dislike': fields.Integer(description='number of dislike on a publication'),
        'publication_type': fields.String(required=True, description='Publication type pictures/text'),
        'user': fields.Nested(UserDto.user_secure),
        'creation_date':  fields.Date(),
        'doIlikeIt': fields.Boolean(default=False, description="for know if the current user push the like"),
        'doIdontlikeIt': fields.Boolean(default=False, description="for know if the current user push the don't like")
    })
    publication_stack = api.model('publication_stack', {
        'status': fields.String(description='request status'),
        'publications': fields.List(fields.Nested(publication), description="list of publication"),
    })
    publication_reaction = api.model('publication_reaction', {
        "user_publication_id": fields.Integer(required=True, description='friend publication id'),
        "reaction": fields.String(required=True, description='Publication reaction type like/dislike')
    })
