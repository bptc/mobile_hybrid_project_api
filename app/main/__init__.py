from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt

from .config import config_by_name

db = SQLAlchemy()
flask_bcrypt = Bcrypt()

HTTP_RESPONSE = {
    "ok": "Done",
    "bad_request": "Bad Request",
    "unauthorized": "Unauthorized",
    "not_found": "Not Found",
    "not_allowed": "Not Allowed",
    "not_acceptable": "Not Acceptable",
    "server_error": "Server Error",
    "token_expired": "Token expired/invalid",
    "conflict": "Resource already existing"
}

HTTP_RESPONSE_CODE = {
    "ok": 200,
    "bad_request": 400,
    "unauthorized": 401,
    "not_found": 404,
    "not_allowed": 405,
    "conflict": 409,
    "not_acceptable": 406,
    "server_error": 500,
    "token_expired": 498
}


def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config_by_name[config_name])
    db.init_app(app)
    flask_bcrypt.init_app(app)
    return app
