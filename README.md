# mobile_hybrid_project_api

## install dependency (use venv if necessary)


 `pip install -r requierement.txt`


## run the serve

`py entry.py run`

## updating db change
`python entry.py db migrate --message 'message'`
`python entry.py db upgrade`

## run unit test
`py entry.py test`