FROM python:3

WORKDIR /app

ADD . /app

RUN pip install -r requirement.txt
#RUN cat requirement.txt | xargs -n 1 pip install

expose 5000

CMD ["python",  "entry.py", "run"]