import os
import unittest

print("env is => {}".format(os.getenv('BOILERPLATE_ENV')))

MODE = os.getenv('BOILERPLATE_ENV') or 'dev'

os.environ["BOILERPLATE_ENV"] = MODE

print("mode is => {}".format(MODE))

from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
from app import blueprint
from app.main import create_app, db

from app.main.model import user
from app.main.model import user_publication


if __name__ == '__main__':
    app = create_app(MODE)

    app.register_blueprint(blueprint)

    app.app_context().push()

    manager = Manager(app)

    migrate = Migrate(app, db)

    manager.add_command('db', MigrateCommand)


    @manager.command
    def run():
        app.run(host='0.0.0.0', port=5000)


    @manager.command
    def test():
        tests = unittest.TestLoader().discover('app/test', pattern='test*.py')
        result = unittest.TextTestRunner(verbosity=2).run(tests)
        if result.wasSuccessful():
            return 0
        return 1

    manager.run()